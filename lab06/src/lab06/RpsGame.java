//Thomas Comeau 1934037
package lab06;

import java.util.Random;

public class RpsGame {
	private int wins = 0;
	private int ties = 0;
	private int losses = 0;
	Random r = new Random();
	
	public RpsGame() {
		
	}
	
	public int getWins() {
		return wins;
	}
	public int getTies() {
		return ties;
	}
	public int getLosses() {
		return losses;
	}
	
	public String playRound(String choice) {
		int computer = r.nextInt(3);
		String comp;
		if(computer == 0) {
			comp = "rock";
		}
		else if(computer == 1) {
			comp = "paper";
		}
		else {
			comp = "scissors";
		}
		if(choice.equals(comp)) {
			ties++;
			return "Computer plays: "+comp+". It is a tie";
		}
		else if(choice.equals("rock")) {
			if(comp.equals("paper")) {
				losses++;
				return "Computer plays: "+comp+". The computer wins";
			}
			else {
				wins++;
				return "Computer plays: "+comp+". The player has won";
			}
		}
		else if(choice.equals("paper")) {
			if(comp.equals("scissors")) {
				losses++;
				return "Computer plays: "+comp+". The computer wins";
			}
			else {
				wins++;
				return "Computer plays: "+comp+". The player has won";
			}
		}
		else if(choice.equals("scissors")) {
			if(comp.equals("rock")) {
				losses++;
				return "Computer plays: "+comp+". The computer wins";
			}
			else {
				wins++;
				return "Computer plays: "+comp+"The player has won";
			}
		}
		else {
			return "Invalid input";
		}
	}
}
