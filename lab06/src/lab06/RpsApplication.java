//Thomas Coemau 1934037
package lab06;

import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;



public class RpsApplication extends Application {
	private RpsGame game = new RpsGame();
	public void start(Stage stage) {
		Group root = new Group(); 
		//scene is associated with container, dimensions
		Scene scene = new Scene(root, 715, 300); 
		scene.setFill(Color.BLACK);
		
		HBox hbox = new HBox();
		Button rock = new Button("Rock");
		Button paper = new Button("Paper");
		Button scissors = new Button("Scissors");
		hbox.getChildren().addAll(rock, paper, scissors);
		
		HBox hbox2 = new HBox();
		TextField welcome = new TextField("Welcome!");
		TextField wins = new TextField("Wins: 0");
		TextField ties = new TextField("Ties: 0");
		TextField losses = new TextField("losses: 0");
		welcome.setPrefWidth(260);
		hbox2.getChildren().addAll(welcome, wins, ties, losses);
		VBox vbox = new VBox();
		vbox.getChildren().addAll(hbox,hbox2);
		root.getChildren().add(vbox);
		RpsChoice rpsChoice = new RpsChoice(welcome,wins,ties,losses,"rock",game);
		RpsChoice rpsChoice1 = new RpsChoice(welcome,wins,ties,losses,"paper",game);
		RpsChoice rpsChoice2 = new RpsChoice(welcome,wins,ties,losses,"scissors",game);
		rock.setOnAction(rpsChoice);
		paper.setOnAction(rpsChoice1);
		scissors.setOnAction(rpsChoice2);

		//associate scene to stage and show
		stage.setTitle("Rock Paper Scissors"); 
		stage.setScene(scene); 
		
		stage.show(); 
	}
	
    public static void main(String[] args) {
        Application.launch(args);
    }
}
