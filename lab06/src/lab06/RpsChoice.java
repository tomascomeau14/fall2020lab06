//Thomas Comeau 1934037
package lab06;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.TextField;

public class RpsChoice implements EventHandler<ActionEvent>{
	private TextField message;
	private TextField wins;
	private TextField ties;
	private TextField losses;
	private String choice;
	private RpsGame game;

	public RpsChoice(TextField message, TextField wins, TextField ties, TextField losses, String choice, RpsGame game) {
		this.message = message;
		this.wins = wins;
		this.ties = ties;
		this.losses = losses;
		this.choice = choice;
		this.game = game;
	}
	
	
	
	@Override
	public void handle(ActionEvent e) {
		// TODO Auto-generated method stub
		String mess = game.playRound(choice);
		message.setText(mess);
		wins.setText("Wins: "+game.getWins());
		ties.setText("Ties: "+game.getTies());
		losses.setText("Losses: "+game.getLosses());
	}
}
